# TP Dockerfile

Nombre y Apellido: Nicolás Francisco You
Materia: Infraestructura de Servidores

## Introducción

El siguiente documento describe los pasos realizados para el laboratorio de Dockerfile.

Consiste en dos webservers Apache2 y un balanceador de carga/proxy reverso Nginx. Los tres recursos configurados como imagen Dockerfile y desplegados con docker-compose.

## Requisitos

Se ha utilizado las instancias EC2 del sandbox environment de AWS para configurar y probar las imágenes.
Para el repositorio, Git Bash en Windows 11.

### SO

- Ubuntu 22.04 LTS

### Red

- IP y DNS por defecto
- Reglas de puertos:
    - SSH (22)
    - HTTP (80)
    - Custom TCP (8000 a 9000)

### Instalación de Docker y Docker Compose

Ya que las instancias son efímeras (3hs), se requiere actualizar el SO e instalar Docker y Docker Compose. Se ha intentado (sin éxito) automatizar este procedimiento con un script alojado en pastebin y ejecutarlo con curl.

~~~bash
# curl -s, --silent
# curl -L, --location
# tr por el End of Line en Windows
$ curl -sL https://pastebin.com/raw/2LQt7ANZ | tr -d '\r' | bash
~~~

Ubuntu pide interacción humana para elegir los servicios a reiniciar. 
2 maneras de hacer el proceso no interactivo:

1. Exportar las variables de entorno
~~~bash
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=critical
# sudo -E (preserve environment)
# apt-get -q (quiet), -y (yes), -o (option), Dpkg ver fuentes
sudo -E apt-get -qy update
sudo -E apt-get -qy -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade
sudo -E apt-get -qy autoclean
~~~
2. Eliminar needrestart
~~~bash
sudo apt-get remove needrestart
sudo apt-get update && sudo apt-get upgrade -y
~~~
Sin embargo, no resultaron útiles. Se ha optado por una actualización e instalación manual.
~~~bash
# actualizar el SO
$ sudo apt update && sudo apt upgrade -y
# instalar docker y docker compose
$ sudo apt install -y docker.io
$ sudo apt install -y docker-compose
# agregar el usuario actual al grupo docker. De esta manera no se requiere sudo para ejecutar el comando docker
$ sudo usermod -aG docker $USER
# Se debe reiniciar la sesión para reflejar cambios de grupo
~~~


## Desarrollo

### 1. Configuración de los archivos de imagen

#### Árbol de directorios del proyecto

~~~bash
lab/
|--> docker-compose.yaml
|--> app1/
    |--> Dockerfile
    |--> html/
        |--> index.html
        |--> styles.css
|--> app2/
    |--> Dockerfile
    |--> html/
        |--> index.html
        |--> styles.css
|--> load-balancer/
    |--> Dockerfile
    |--> nginx.conf
    |--> sites-available/
        |--> upstream.conf
        |--> default.conf
~~~

#### Creación de directorios

~~~bash
~/miercoles-infraestructuras (main)
$ mkdir -p lab/{app1/html,app2/html,load-balancer/sites-available}
$ cd lab
~~~

#### Configuración de Apache2

Las app1 y app2 son webserver Apache2 con contenido estático en html.

~~~bash
$ sudo vi app1/html/index.html
~~~
~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App 1</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>¡BIENVENIDO A LA APP 1!</h1>
    <p>Sitio web estático de prueba.</p>
    <p>Powered by Apache2.</p>
</body>
</html>
~~~
~~~bash
$ sudo vi app1/html/styles.css
~~~
~~~css
body {
    font-family: Arial, sans-serif;
    text-align: center;
    margin-top: 50px;
}

h1 {
    color: #333;
}

p {
    color: #666;
}
~~~
Hacer lo mismo para app2.

#### Dockerfile para app1 y app2

~~~bash
$ sudo vi app1/Dockerfile
~~~
~~~dockerfile
# imagen base Ubuntu 
FROM ubuntu

# instalar apache
RUN apt-get update && apt-get install -y apache2

# copiar el contenido estático al directorio default de Apache
COPY ./html /var/www/html

# abrir puertos del conteneder
EXPOSE 80

# comandos para iniciar servicio Apache2. [ -D parameter ] inicia el servicio en foreground.
CMD ["apachectl", "-D", "FOREGROUND"]
~~~

#### Configuración de Nginx
Debajo están las configuraciones para el Nginx:
- archivo base
- balanceador de carga
- proxy reverso

Son las que terminaron funcionando. Más adelante se detalla los errores enfrentados.

Archivo base:
~~~bash
$ sudo vi load-balancer/nginx.conf
~~~
~~~nginx
# configuración default
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    # agregar los archivos de configuración para balanceo de carga/proxy reverso
    # include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/conf.d/upstream.conf;
    include /etc/nginx/conf.d/default.conf;
}
~~~

Load balancer:
~~~bash
$ sudo vi load-balancer/sites-available/upstream.conf
~~~
~~~nginx
# configuración de load balancer, algoritmo default (round robin)

upstream webserver {
    server app1:80;
    server app2:80;
}
~~~

Reverse proxy:
~~~bash
$ sudo vi load-balancer/sites-available/default.conf
~~~
~~~nginx
server {
    listen 80;
    # en server_name se utilizó el dns público de la instancia EC2
    server_name dns-de-instancia-aws.com;

    location / {
        proxy_pass http://webserver;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
~~~

Dockerfile para el Nginx:
~~~bash
$ sudo vi load-balancer/Dockerfile
~~~
~~~dockerfile
# imagen base nginx
FROM nginx

# copiar los archivos de configuración
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./sites-available/upstream.conf /etc/nginx/conf.d/upstream.conf
COPY ./sites-available/default.conf /etc/nginx/conf.d/default.conf

# abrir puerto 80
EXPOSE 80

# iniciar el servicio Nginx. [-g directives] daemon off = inicia el servicio en foreground
CMD ["nginx", "-g", "daemon off;"]
~~~

### 2. Configuración de docker-compose.yaml 
~~~bash
sudo vi docker-compose.yaml
~~~
~~~yaml
version: '3.8'

# Definición de los 3 servicios

services:
  app1:
    build:
      context: ./app1
    container_name: app1
    ports:
      - "8081:80"
    networks:
      - red1

  app2:
    build:
      context: ./app2
    container_name: app2
    ports:
      - "8082:80"
    networks:
      - red1

  load-balancer:
    build:
      context: ./load-balancer
    container_name: load-balancer
    ports:
      - "80:80"
    depends_on:
      - app1
      - app2
    networks:
      - red1

networks:
  red1:
~~~

### 3. Despliegue de servicios

Armar y desplegar los servicios:
~~~bash
# -d, detached
$ docker-compose up -d --build
~~~

### 4. Comprobar acceso

El sitio debería mostrar el contenido de app1 y app2 balanceadas por Nginx.

En el navegador:
~~~
http://dns-de-la-instancia.com
http://ip.publica.de.instancia

Por ej:
http://ec2-34-239-187-183.compute-1.amazonaws.com/
~~~

![prueba1](img/prueba1.png "Prueba1")
![prueba2](img/prueba2.png "Prueba2")

En el mismo servidor (instancia EC2):
~~~bash
$ curl http://localhost
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App 1</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>¡BIENVENIDO A LA APP 1!</h1>
    <p>Sitio web estático de prueba.</p>
    <p>Powered by Apache2.</p>
</body>
</html>
...
$ curl http://localhost
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App 2</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>¡BIENVENIDO A LA APP 2!</h1>
    <p>Sitio web estático de prueba.</p>
    <p>Powered by Apache2.</p>
</body>
</html>
~~~

### 5. Errores

Para revisar los logs se puede utilizar:
~~~bash
$ docker-compose logs app1
$ docker-compose logs app2
$ docker-compose logs load-balancer
~~~

O bien observar la salida cuando falla el build.

#### Configuración

Durante las pruebas de configuración se observaron errores con el Nginx.

~~~bash
$ docker-compose up --build 
Successfully tagged my-web-app_load-balancer
Creating app1 ... done
Creating app2 ... done
Creating load-balancer ... done
Attaching to app1, app2, load-balancer
app1 | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.18.0.2. Set the 'ServerName' directive globally to suppress this message
app2 | AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.18.0.3. Set the 'ServerName' directive globally to suppress this message
load-balancer | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
load-balancer | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
load-balancer | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
load-balancer | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
load-balancer | 10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
load-balancer | /docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
load-balancer | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
load-balancer | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
load-balancer | /docker-entrypoint.sh: Configuration complete; ready for start upload-balancer | 2024/06/19 03:38:57 [emerg] 1#1: "upstream" directive is not allowed here in /etc/nginx/nginx.conf:1
load-balancer | nginx: [emerg] "upstream" directive is not allowed here in /etc/nginx/nginx.conf:1
load-balancer exited with code 1
~~~

La configuración original de ~/lab/load-balancer/sites-available/default.conf contenía el bloque de upstream en el mismo archivo. Se optó por definirlo en un archivo aparte.

#### Red

Si bien el servicio de Nginx pudo levantar, no se pudo acceder al sitio.
~~~
Step 1/5 : FROM nginx
---> dde0cca083bc
Step 2/5 : RUN rm /etc/nginx/conf.d/default.conf
---> Running in a448bf627579
Removing intermediate container a448bf627579
---> 7ee23d9844cc
Step 3/5 : COPY ./nginx.conf /etc/nginx/nginx.conf
---> 982e85a9eb31
Step 4/5 : EXPOSE 80
---> Running in 9233eda29c5f
Removing intermediate container 9233eda29c5f
---> 987bbe6ae784
Step 5/5 : CMD ["nginx", "-g", "daemon off;"]
---> Running in 281dec95c6a3
Removing intermediate container 281dec95c6a3
---> 907348836259
Successfully built 907348836259
Successfully tagged my-web-app_load-balancer
Starting app2 ... done
Starting app1 ... done
Recreating load-balancer ... done

# En el navegador:

http://ec2-54-237-105-210.compute-1.amazonaws.com/
This site can’t be reachedec2-54-237-105-210.compute-1.amazonaws.com refused to connect.
Try:

Checking the connection
Checking the proxy and the firewall
ERR_CONNECTION_REFUSED
~~~

Se asignó el mismo grupo de red para los 3 servicios.
En docker-compose.yaml:
~~~yaml
services:
  app1:
    networks:
      - red1
      ...
  app2:
    networks:
      - red1
      ...
  load-balancer:
    networks:
      - red1

networks:
  red1
~~~

También, se comprobó que la máquina local (Win11) no resolvía el dns de la instancia EC2. Por lo cual se modificó el archivo 'C:\Windows\System32\drivers\etc\hosts'.

~~~
# agregar línea con IP pública y dns público
34.239.187.183		ec2-34-239-187-183.compute-1.amazonaws.com
~~~

## Conclusión

El laboratorio de Dockerfile demuestra cómo desplegar dos webservers Apache2 y un balanceador de carga/proxy reverso Nginx utilizando Docker y Docker Compose. A través de la configuración de archivos Dockerfile y docker-compose.yaml, se logró un entorno funcional.

## Fuentes

### apt-get update && upgrade script

https://askubuntu.com/questions/1367139/apt-get-upgrade-auto-restart-services

https://packages.debian.org/sid/debconf-utils

https://unix.stackexchange.com/questions/107194/make-apt-get-update-and-upgrade-automate-and-unattended

### Dockerfile

https://docs.docker.com/reference/dockerfile/

### Nginx

https://nginx.org/en/docs/ngx_core_module.html#daemon

https://stackoverflow.com/questions/75636748/why-use-nginx-g-daemonf-off-in-dockerfiles-for-nginx

https://stackoverflow.com/questions/53454739/nginx-upstream-failure-configuration-file

https://stackoverflow.com/questions/51658198/nginx-emerg-upstream-directive-is-not-allowed-here-in-etc-nginx-sites-enab

### Adicionales

- Pads de la materia
- man page de los comandos

### Sintaxis del Markdown de Gitlab

https://docs.gitlab.com/ee/user/markdown.html